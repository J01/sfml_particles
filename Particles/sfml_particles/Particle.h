#if !defined(ParticleH)
#define ParticleH

#include "CartVec.h"

//  Structure representing a physical particle 
//  Contains functions to move itself and the data related to this

struct Particle
{
    CartVec v = { 0,0 };            // velocity
    CartVec s = { 0, 0 };           // displacement i.e. position

    float m = 1.f;                  // mass 
    float e = 0.9f;                 // coefficient of restitution for collisions

    float r = 1.f;                  // radius
        
    // Function to update and move the particle
    // IN   : elapsed, time in last frame
    void Update(const float elapsed); 
};

// Collection of functions to handle collisions between particles
namespace ParticleCollision
{
    // Function to check if two particles are colliding
    // OUT  : bool
    //IN/OUT: p1, p2 both particles
    // post : true returned if particles are colliding
    bool CheckCollision(const Particle& p1, const Particle& p2);

    // Function to resolve the collision between two particles
    //IN/OUT: p1, p2 both particles
    void Collide(Particle& p1, Particle& p2);
}

#endif 
