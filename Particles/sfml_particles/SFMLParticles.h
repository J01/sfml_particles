#if !defined(SFMLParticlesH)
#define SFMLParticlesH

#include "SFML/Graphics.hpp"
#include "ParticleGroup.h"
#include "ColourShiftStruct.h"

class SFMLParticles : public ParticleGroup
{
public:

    // Draw all the particles
    void Draw(sf::RenderWindow& window);

    // Use a single circle shape to stamp particles where necessary
    sf::Sprite particleStamp;
    sf::Texture circleTexture;
    // Used to create colour shift effect.
    ColourShift shiftEffect;
private:

    void SetEffect();
};

#endif