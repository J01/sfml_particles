#pragma once

#include "SFMLParticles.h"

void SetupWavyWaterfall(SFMLParticles& sfParts);

void SetupFire(SFMLParticles& sfParts);