#include "EmittersSetup.h"

#include <assert.h>

void SetupWavyWaterfall(SFMLParticles& sfParts)
{
	sfParts.Initialise(150.f, 5.f);

	if (sfParts.circleTexture.loadFromFile("circle.png") == false)
		assert(false);
	sfParts.particleStamp.setTexture(sfParts.circleTexture);
	sfParts.particleStamp.setOrigin(sfParts.particleStamp.getGlobalBounds().width / 2.f, sfParts.particleStamp.getGlobalBounds().height / 2.f);

	sfParts.myPosition = { 150, 300 };
	sfParts.startVelocity = 200.f;
	sfParts.myDirection = { 1.f, -0.25f };
	sfParts.myDirection.Normalise();
	sfParts.emissionXVariation = 0.2f;
	sfParts.genrtdGravity *= 2.f;

	sfParts.scaleYs.resize(12);
	for (int i = 0; i < 11; ++i)
	{
		sfParts.scaleYs[i].at = i * 1.5f;
		sfParts.scaleYs[i].scale = (i % 2) ? 1.f : 0.5f;
	}
	sfParts.scaleXs[0].scale = 0.5f;
	sfParts.scaleXs[1].scale = 1.5f;


	ColourShift shiftEffect
	{
		0,		3,
		0,		3,
		255,	3,
		255,	5
	};
	sfParts.shiftEffect = shiftEffect;
}

void SetupFire(SFMLParticles& sfParts)
{
	sfParts.Initialise(30.f, 1.5f);

	if (sfParts.circleTexture.loadFromFile("circle.png") == false)
		assert(false);
	sfParts.particleStamp.setTexture(sfParts.circleTexture);
	sfParts.particleStamp.setOrigin(sfParts.particleStamp.getGlobalBounds().width / 2.f, sfParts.particleStamp.getGlobalBounds().height / 2.f);
	sfParts.startVelocity = 120.f;
	sfParts.myDirection = { 0.f, -1.f};
	sfParts.myDirection.Normalise();
	sfParts.emissionXVariation = 0.2f;
	sfParts.genrtdGravity = 3.f;


	sfParts.scaleXs[0].scale = 2.f;
	sfParts.scaleXs[1].scale = 0.125f;
	sfParts.scaleYs[0].scale = 2.5f;
	sfParts.scaleYs[1].scale = 0.125f;

	ColourShift shiftEffect
	{
		200,	50,
		0,		3,
		0,		3,
		0,		5
	};
	sfParts.shiftEffect = shiftEffect;
}
