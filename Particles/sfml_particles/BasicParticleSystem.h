#if !defined(BasicParticleSystemH)
#define BasicParticleSystemH

#include "ParticleGroup.h"

class BasicParticleSystem
{
public:
    BasicParticleSystem();
    ~BasicParticleSystem();

    void Update(float elapsed);

private:
    ParticleGroup basicGroup;
};

#endif