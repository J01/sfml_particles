#include "ParticleGroup.h"

#include "RandomNumberGenerator.h"

#include <assert.h>

ParticleGroup::ParticleGroup()
{}
ParticleGroup::~ParticleGroup()
{}

void ParticleGroup::Initialise(float particlesPerSecond, float particleLifetime)
{
    // round up for number needed
    int numberOfParticlesNeeded;
    numberOfParticlesNeeded = ceilf(particlesPerSecond * particleLifetime) + 1;
    assert(numberOfParticlesNeeded >= 0);

    // Resize the vectors
    startPosition_.resize(numberOfParticlesNeeded);
    position_.resize(numberOfParticlesNeeded);
    velocity_.resize(numberOfParticlesNeeded);
    scale_.resize(numberOfParticlesNeeded);
    rotation_.resize(numberOfParticlesNeeded);
    timeSinceCreate_.resize(numberOfParticlesNeeded);

    // Setup internal data
    particleLifetime_ = particleLifetime;
    timeBetweenParticles_ = 1.f / particlesPerSecond;
    scaleXs.resize(2);
    scaleYs.resize(2);
    scaleXs[0].at = 0.f;
    scaleXs[1].at = particleLifetime;
    scaleXs[0].scale = 1/32.f;
    scaleXs[1].scale = 1/32.f;
    scaleYs[0].at = 0.f;
    scaleYs[1].at = particleLifetime;
    scaleYs[0].scale = 1/32.f;
    scaleYs[1].scale = 1/32.f;
}

void ParticleGroup::Update(float elapsed)
{
    UpdateSpawns(elapsed);
    UpdateMoves(elapsed);
}


void ParticleGroup::RemoveAt(int atIdx)
{
    // Check atIdx is in appropriate range
    assert(atIdx >= 0 && atIdx < count);

    // Resize active group
    --count;

    // Move the back element into atIdx
    startPosition_[atIdx] = startPosition_[atIdx];
    position_[atIdx] = position_[count];
    velocity_[atIdx] = velocity_[count];
    scale_[atIdx] = scale_[count];
    rotation_[atIdx] = rotation_[count];
    timeSinceCreate_[atIdx] = timeSinceCreate_[count];
}

void ParticleGroup::UpdateMoves(float elapsed)
{
    for (int i = 0; i < count; ++i)
    {
        // update time and destroy if necessary
        timeSinceCreate_[i] += elapsed;
        if (timeSinceCreate_[i] > particleLifetime_)
        {
            RemoveAt(i);
            --i;
            return;
        }

        // s = ut + 0.5at^2
        float tSC = timeSinceCreate_[i];
        position_[i] = startPosition_[i] + velocity_[i] * tSC;
        position_[i].j += 0.5f * genrtdGravity * tSC * tSC;

        //// find the rotation
        //rotation_[i] = (timeSinceCreate_[i] / particleLifetime_) * lifetimeRotation;

        // find new scale by using key frame lerp
        bool xFound = false;
        int scaleIdx = -1;
        assert(scaleXs.size() >= 2);
        do
        {
            ++scaleIdx;
            xFound = timeSinceCreate_[i] <= scaleXs[scaleIdx].at;
        } while (!xFound);
        // time in frame / total time in frame
        float alpha = (timeSinceCreate_[i] - scaleXs[scaleIdx - 1].at) / (scaleXs[scaleIdx].at - scaleXs[scaleIdx - 1].at);
        scale_[i].i = scaleXs[scaleIdx - 1].scale + alpha * (scaleXs[scaleIdx].scale - scaleXs[scaleIdx - 1].scale);

        // scale for ys as well
        bool yFound = false;
        scaleIdx = -1;
        assert(scaleYs.size() >= 2);
        do
        {
            ++scaleIdx;
            yFound = timeSinceCreate_[i] <= scaleYs[scaleIdx].at;
        } while (!yFound);
        // time in frame / total time in frame
        alpha = (timeSinceCreate_[i] - scaleYs[scaleIdx - 1].at) / (scaleYs[scaleIdx].at - scaleYs[scaleIdx - 1].at);
        scale_[i].j = scaleYs[scaleIdx - 1].scale + alpha * (scaleYs[scaleIdx].scale - scaleYs[scaleIdx - 1].scale);
    }
}
void ParticleGroup::UpdateSpawns(float elapsed)
{
    // find number to create and adjust time since accordingly
    timeSinceSpawn_ += elapsed;

    int numToCreate = static_cast<int>(timeSinceSpawn_ / timeBetweenParticles_);
    timeSinceSpawn_ -= static_cast<float>(numToCreate) * timeBetweenParticles_;

    if (numToCreate + static_cast<int>(count) > static_cast<int>(position_.size()))
        numToCreate = static_cast<int>(position_.size()) - static_cast<int>(count) - 2;

    // Create each particle
    for (int i = 0; i < numToCreate; ++i)
    {
        // set the position
        startPosition_[count].i = myPosition.i + RandomNumberGenerator::fRandom(-outDistX, outDistX);
        startPosition_[count].j = myPosition.j + RandomNumberGenerator::fRandom(-outDistY, outDistY);

        // find velocity with added randomness
        velocity_[count].i = startVelocity * (myDirection.i + RandomNumberGenerator::fRandom(-emissionXVariation, emissionXVariation));
        velocity_[count].j = startVelocity * (myDirection.j + RandomNumberGenerator::fRandom(-emissionYVariation, emissionYVariation));
       
        scale_[count].i = scaleXs[0].scale;
        scale_[count].j = scaleYs[0].scale;
        timeSinceCreate_[count] = 0.f;

        // extend group
        ++count;
    }
}