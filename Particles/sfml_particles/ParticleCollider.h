#if !defined(ParticleColliderH)
#define ParticleColliderH

#include "Particle.h"
#include "CartVec.h"

#include "SFML/Graphics.hpp"

namespace ParticleColliderConsts
{
    const int minV = 60, maxV = 200;        // min and max velocity at init time
    const int minM = 3, maxM = 10;          // min and max mass 
    const int minR = 10, maxR = 30;         // min and max radius

    const size_t numberParticles = 30;      // number of particles to use in the simulation
}

// Structure to hold the particle collision simulation

struct ParticleCollider
{
    Particle p[ParticleColliderConsts::numberParticles];
    const float LeftBound = 20, RightBound = 580, TopBound = 20, BottomBound = 580;

    // Function to initialise the particle simulation
    void Initialise();
    // Function to update the particle simulation
    // IN   : elapsed, time in last frame
    void Update(const float elapsed);
    // Function to draw the particle simulation
    // IN/OUT: window, the RenderWindow to draw to
    void Draw(sf::RenderWindow& window) const;


    // Function to draw a particle to a RenderWindow
    //IN/OUT: particle and RenderWindow
    void DrawP(const Particle& p, sf::RenderWindow& window) const;

    //jury rig a box which contains all the circles, and in which they bounce
    void CheckCollisionsWithBox(Particle& p);
};


#endif