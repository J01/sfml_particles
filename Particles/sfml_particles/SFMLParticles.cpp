#include "SFMLParticles.h"


void SFMLParticles::Draw(sf::RenderWindow& window)
{
    particleStamp.setColor(sf::Color(255, 0, 0, 255));
    sf::RenderStates rStates;
    rStates.blendMode = sf::BlendAlpha;

    for (int p = 0; p < count; ++p)
    {
        particleStamp.setScale(scale_[p]);
        particleStamp.setPosition(position_[p]);
        SetEffect();
        window.draw(particleStamp, rStates);
    }
}

void SFMLParticles::SetEffect()
{
    //start colour + rate of change.
    particleStamp.setColor(sf::Color(       shiftEffect.red     + particleStamp.getPosition().y / shiftEffect.redRateOfChange,
                                            shiftEffect.green   + particleStamp.getPosition().y / shiftEffect.greenRateOfChange,
                                            shiftEffect.blue,
                                            shiftEffect.alpha   + particleStamp.getPosition().y / shiftEffect.alphaRateOfChange));
}