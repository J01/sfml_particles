#pragma once

/*
	Set colour change effects using starting value and
	rate of change for rgba sf::color.

	std::uint8_t values in order:

	Base red value,		rate of red change
	Base blue value,	rate of blue change
	Base green value,	rate of green change
	Base alpha value,	rate of alpha change
*/
struct ColourShift
{
	std::uint8_t red,			redRateOfChange;
	std::uint8_t green,			greenRateOfChange;
	std::uint8_t blue,			blueRateOfChange;
	std::uint8_t alpha,			alphaRateOfChange;
};