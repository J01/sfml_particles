#if !defined(RandomNumberGeneratorH)
#define RandomNumberGeneratorH

// Jake Johnson Powell, January 2021

// A class to generate random numbers
//  This is an abstract class that should not be instantiated, just a collection of static functions

class RandomNumberGenerator
{
public :
    RandomNumberGenerator() = delete;
    RandomNumberGenerator(const RandomNumberGenerator&) = delete;
    void operator=(const RandomNumberGenerator&) = delete;

    // << update >>
    static void Seed();
    static void Seed(unsigned int s);

    // << query >>
    // Function to generate a random int number between 0 and max
    // IN   : max val of generated num
    // OUT  : value generated
    // pre  : max >= 0
    // post : 0 <= out <= max
    static int iRandom(unsigned int max); 
    // Function to generate a random int number between min and max
    // IN   : min and max
    // OUT  : value generated
    // pre  : min <= max
    // post : min <= out <= max
    static int iRandom(int min, int max);

    // Function to generate a random float number between 0 and max
    // IN   : max val of generated num
    // OUT  : value generated
    // pre  : max >= 0
    // post : 0 <= out <= max
    static float fRandom(float max);
    // Function to generate a random float number between min and max
    // IN   : max val of generated num
    // OUT  : value generated
    // pre  : min <= max
    // post : 0 <= out <= max
    static float fRandom(float min, float max);

private:
};

#endif