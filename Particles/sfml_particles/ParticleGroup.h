#if !defined(ParticleGroupH)
#define ParticleGroupH

#include <vector>
#include "CartVec.h"

// Data oriented particles
// Particles are spawned in random positions in a given 2D box, then assigned a random velocity in a range

struct ScaleKFrame
{
    float at;           // when is it at this scale 0 to lifetime
    float scale;        // what scale is it at 0 to 1
};

class ParticleGroup
{
public:
    ParticleGroup();
    ~ParticleGroup();

    // Take particlesPerSecond and particleLifetime as arguments here 
    // so we can work out how many particles to allocate
    void Initialise(float particlePerSecond, float particleLifetime);



    // Update movement
    void Update(float elapsed);

    // Emitter settings
    CartVec     myPosition;
    CartVec     myDirection = { 1,0 };
    CartVec     basicScale = { 1,1 };
    float       startVelocity = 1.f;            // speed at which to emit
    float       genrtdGravity = 9.8f;           // gravity constant to use
    float       emissionXVariation = 0.f;       // speed variation in x axis
    float       emissionYVariation = 0.f;       // speed variation in y axis
    float       outDistX = 0.f;                 // maximum x position variance  
    float       outDistY = 0.f;
    float       lifetimeRotation = 0.f;         // rotation over course of lifetime

    // Data used for creating particles ; publicly available to be changed
    std::vector<ScaleKFrame> scaleXs;
    std::vector<ScaleKFrame> scaleYs;

    /*
        Create mechanic for update rotation
    */

protected:
    size_t  count = 0;
    //
    // Some free unpadded space here
    //

    // Data oriented particles
    std::vector<CartVec> startPosition_;
    std::vector<CartVec> position_;
    std::vector<CartVec> velocity_;
    std::vector<CartVec> scale_;
    std::vector<float>   rotation_;
    std::vector<float>   timeSinceCreate_;
private:

    float timeSinceSpawn_ = 0.f;               // time since last particle was created
    float particleLifetime_ = 0.f;              // lifetime of a particle
    float timeBetweenParticles_ = 0.f;          // time between particles being generated


    // Remove a particle at a given index from the active group
    void RemoveAt(int idx);

    // Internal update functions
    void UpdateMoves(float elapsed);
    void UpdateSpawns(float elapsed);
};

#endif 
