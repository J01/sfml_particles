#include <math.h>
#include "SFML/Graphics.hpp"

#if !defined(CartVecH)
#define CartVecH

// Structure representing a cartesian vector, position or otherwise

struct CartVec
{
    // Values ij, xy
    float i = 0, j = 0;


    // Function to normalise the vector
    // post : magnitude of vector will be 1
    void Normalise();
    // Function to get the perpendicular vector to this
    // OUT  : normal vector to this
    CartVec GetPerp() const;

    // Function to get the scalar product with another CartVec
    //  OUT : float, scalar product
    //IN/OUT: other CartVec
    float dotProd(const CartVec& other) const;


    inline CartVec operator +(const CartVec& other) const
    {
        return { i + other.i, j + other.j };
    }
    inline CartVec operator -(const CartVec& other) const
    {
        return { i - other.i, j - other.j };
    }

    inline CartVec operator *(const float& other) const
    {
        return { other * i, other * j };
    }
    inline CartVec operator *(const int& other) const
    {
        return { i * other, j * other };
    }

    inline CartVec operator /(const float& other) const
    {
        return { i / other, j / other };
    }
    inline CartVec operator /(const int& other) const
    {
        return { i / static_cast<float>(other), j / static_cast<float>(other) };
    }

    inline CartVec& operator =(const CartVec& other)
    {
        i = other.i;
        j = other.j;
        return *this;
    }
    inline bool operator ==(const CartVec& other) const
    {
        return (i == other.i && j == other.j);
    }

    operator sf::Vector2f& ()
    {
        return sf::Vector2f(i, j);
    }
};


#endif