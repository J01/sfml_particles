#include "RandomNumberGenerator.h"

#include <cstdlib>                                      //  for srand and rand  
#include <ctime>                                        //  for time
#include <assert.h>                                     //  for pre and post conditions

void RandomNumberGenerator::Seed()
{
    srand(static_cast<unsigned int>(time(0)));
}
void RandomNumberGenerator::Seed(unsigned int s)
{
    srand(s);
}

int RandomNumberGenerator::iRandom(unsigned int max)
{
    assert(max >= 0);
    int returnVal = rand() % (max + 1);
    assert(0 <= returnVal && returnVal <= (int)max);
    return returnVal;
}
int RandomNumberGenerator::iRandom(int min, int max)
{
    assert(min <= max);
    int range = max - min;
    int returnVal = (rand() % (range + 1)) + min;
    assert(min <= returnVal && returnVal <= max);
    return returnVal;
}
float RandomNumberGenerator::fRandom(float max)
{
    assert(max >= 0);
    float randomGen = (float)rand() / (float)RAND_MAX;    // get a decimal such that 0 <= x <= 1
    float returnVal = max * randomGen;
    assert(0 <= returnVal && returnVal <= max);
    return returnVal;
}
float RandomNumberGenerator::fRandom(float min, float max)
{
    assert(min <= max);
    float randomGen = (float)rand() / (float)RAND_MAX;    //get a decimal such that 0 <= x <= 1
    float range = max - min;
    float returnVal = (range * randomGen) + min;
    assert(min <= returnVal && returnVal <= max);
    return returnVal;
}