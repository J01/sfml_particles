#include "SFML/Graphics.hpp"
#include "ParticleCollider.h"
#include "SFMLParticles.h"
#include "ColourShiftStruct.h"
#include "EmittersSetup.h"

#include <assert.h>


int main()
{
	// Create the main window
	sf::RenderWindow window(sf::VideoMode(1280, 850), "Particle System Demonstration");


	SFMLParticles sfParts;
	SetupWavyWaterfall(sfParts);

	const int numFires = 6;
	SFMLParticles sfFire[numFires];
	for (int i = 0; i < numFires; ++i)
	{
		SetupFire(sfFire[i]);
		sfFire[i].myPosition = CartVec{ 950.f + (float)i * 35.f, 300.f };
	}

    sf::Clock clk;
    clk.restart();
	
    float elapsed;

	// Start the game loop 
	while (window.isOpen())
	{
		// Process events
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) 
				window.close();
		} 

		// Clear screen
		window.clear();

        elapsed = clk.getElapsedTime().asSeconds();
        clk.restart();

		sfParts.Update(elapsed);
		sfParts.Draw(window);
		for (int i = 0; i < numFires; ++i)
		{
			sfFire[i].Update(elapsed);
			sfFire[i].Draw(window);
		}

		// Update the window
		window.display();

	}

	return 0;
}
