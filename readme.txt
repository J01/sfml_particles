This small project sets out to create a simple, somewhat efficient particle
system, with an example implementation SFML, but constructed such that the
system is independent of the rendering method.

Due to the simple nature of this project, and its short timeframe, there will
be no design documents provided.

A secondary aim of this work is to demonstrate the competency 'using git in a
team', and to demonstrate the ability of both students to implement a particle
system.